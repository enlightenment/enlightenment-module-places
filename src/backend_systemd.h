#ifndef E_MOD_PLACES_SYSTEMD_H
#define E_MOD_PLACES_SYSTEMD_H

Eina_Bool places_systemd_init(void);
void places_systemd_shutdown(void);

#endif
